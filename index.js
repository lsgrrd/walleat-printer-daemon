// iker se la come
const escpos = require ('escpos')
const express = require('express')
const cors = require('cors')
const path = require('path');
var dayjs = require('dayjs')
// install escpos-usb adapter module manually
escpos.USB = require('escpos-usb');
// Select the adapter based on your printer type
const device  = new escpos.USB();
// const device  = new escpos.Network('localhost');
// const device  = new escpos.Serial('/dev/usb/lp0');

const options = { width: 42 }
// encoding is optional

const printer = new escpos.Printer(device, options);

device.open(function(error){
  printer
  .font('a')
  .align('ct')
  .style('bu')
  .size(1, 1)
  .text('Walleat')
  .font('b')
  .text('Punto de venta')
  .cut()
  .close()
});

let print = (message) => {
  return new Promise((resolve, reject) => {
    device.open(function(error){
      printer
        .font('b')
        .align('lt')
        .style('normal')
        .size(0.5, 0.5)
        .text(message)
        .drawLine()
        .tableCustom(
          [
            { text:"Left", align:"LEFT", width:0.8 },
            { text:"Right", align:"RIGHT", width:0.2 }
          ]
        )
        .size(1, 1)
        .qrimage(message, function(err){
          this.close()
        })

      resolve('ok')
    })
  })
}

let ticket = (order) => {
  return new Promise((resolve) => {
    device.open(function () {
      printer
        .font('b')
        .align('ct')
        .size(.7,.7)
        .text(order.store)
	.size(0.5, 0.5)
	.text(' ')
        .size(0.5, 0.5)
        .text(dayjs(order.date).format('DD/MMM/YY HH:mm'))
	.text(order.is_refund ? 'DEVOLUCION' : '')
	.text(order.order_type.type === 'sell' ? 'Venta' : 'Compra de inventario')
        .text(order.order_type.payment === 'cash' ? 'Efectivo' : 'Tarjeta')
        .align('lt')
	.size(0.5, 0.5)
        .drawLine()
      order.products.forEach(product => {
        printer
          .tableCustom(
            [
              { text: `${product.units} - ${product.name} ${product.variant}`, align:"LEFT", width: 0.7 },
              { text: `$${product.unit_price * product.units}`,align:"RIGHT", width: 0.3 },
            ]
          )
      })
      printer
        .drawLine()
       // .tableCustom(
       //   [
       //     { text: 'Subtotal', align:"LEFT", width:0.7, style: 'B' },
       //     { text: `$${order.subtotal.toFixed(2)}`, align:"RIGHT", width:0.3},
       //   ]
       // )
       // .tableCustom(
       //   [
       //     { text: 'IVA', align:"LEFT", width:0.7, style: 'B' },
       //     { text: `$${order.iva.toFixed(2)}`, align:"RIGHT", width:0.3},
       //   ]
       // )
        .tableCustom(
          [
            { text: 'Total', align:"LEFT", width:0.7, style: 'B' },
            { text: `$${order.total.toFixed(2)}`, align:"RIGHT", width:0.3},
          ]
        )
	.drawLine()
	.tableCustom(
          [
            { text: 'Su pago', align:"LEFT", width:0.7, style: 'B' },
            { text: `$${order.cashReceived}`, align:"RIGHT", width:0.3},
          ]
        )
	.tableCustom(
          [
            { text: 'Cambio', align:"LEFT", width:0.7, style: 'B' },
            { text: `$${order.giveBack}`, align:"RIGHT", width:0.3},
          ]
        )
        .control('LF')
        .align('ct')
        .text('GRACIAS')
       	.text('Walleat POS')
	.text('www.mywalleat.com')
        .cashdraw(order.order_type.payment === 'cash' ? 2 : '')
        .control('LF')
        .control('LF')
        .control('LF')
        .close()

    })
    resolve()
  })
}


let ticket_phone_credits = (order) => {
  return new Promise((resolve) => {
    device.open(function () {
      printer
        .font('b')
        .align('ct')
        .size(.7,.7)
        .text(order.store)
	.size(0.5,0.5)
	.text('Transaccion exitosa')
        .size(0.5, 0.5)
        .text(' ')
        .size(0.5, 0.5)
	.text(dayjs(order.date).format('DD/MMM/YY HH:mm'))
        .text(order.order_type.type === 'sell' ? 'Venta de tiempo aire' : ' ')
        .text(order.order_type.payment === 'cash' ? 'Pago en efectivo' : 'Pago con tarjeta')
        .size(0.5, 0.5)
        .drawLine()
	.text('Producto:')
	.text(order.products.name)
	.tableCustom(
            [
              { text: 'Monto:', align:"LEFT", width: 0.7, style: 'B' },
              { text: `$${order.products.unit_price}`, align:"RIGHT", width: 0.3 },
            ]
          )
	.tableCustom(
            [
              { text: 'Autorizacion:', align:"LEFT", width: 0.7, style: 'B' },
              { text: `${order.products.autorization_code}`, align:"RIGHT", width: 0.3 },
            ]
          )
	.tableCustom(
            [
              { text: 'Referencia:', align:"LEFT", width: 0.7, style: 'B' },
              { text: `${order.products.reference}`, align:"RIGHT", width: 0.3 },
            ]
          )
  .tableCustom(
    [
      { text: 'Informacion importante:', align:"LEFT", width: 0.7, style: 'B' },
    ]
  )
	 .text(order.products.important_info)
    	.drawLine()
        .tableCustom(
          [
            { text: 'Total', align:"LEFT", width:0.7, style: 'B' },
            { text: `$${order.total.toFixed(2)}`, align:"RIGHT", width:0.3},
          ]
        )
        .drawLine()
        .control('LF')
        .align('ct')
        .text('GRACIAS')
        .text('Walleat POS')
	.text('www.mywalleat.com')
        .cashdraw(order.order_type.payment === 'cash' ? 2 : '')
        .control('LF')
        .control('LF')
        .control('LF')
        .close()

    })
    resolve()
  })
}

let emida_error = (order) => {
  return new Promise((resolve) => {
    device.open(function () {
      printer
        .font('b')
        .align('ct')
        .size(.7,.7)
        .text(order.store)
        .size(0.5,0.5)
        .text('Transaccion NO exitosa')
        .size(0.5, 0.5)
        .text(' ')
        .size(0.5, 0.5)
        .text(dayjs(order.date).format('DD/MMM/YY HH:mm'))
        .text(order.order_type.type === 'sell' ? 'Venta de tiempo aire' : ' ')
        .text(order.order_type.payment === 'cash' ? 'Pago en efectivo' : 'Pago con tarjeta')
        .size(0.5, 0.5)
        .drawLine()
        .tableCustom(
            [
              { text: 'Motivo:', align:"LEFT", width: 0.7, style: 'B' },
              { text: `$${order.message}`, align:"RIGHT", width: 0.3 },
            ]
          )
    })
   resolve()
  })
}


let openCd = (message) => {
  return new Promise((resolve, reject) => {
    device.open(function(error){
      printer
        .cashdraw(2)
        .close()
      resolve('ok')
    })
  })
}

const app = express()
const port = 3000
app.use(cors())
app.use(express.json())

app.post('/', (req, res) => {
  print(req.body.text)
    .then(response => {
      res.status(200).send({
        status: response
      })
    })
    .catch(err => {
      console.log(err)
    })
})

app.post('/ticket', (req, res) => {
  ticket(req.body.order)
    .then(response => {
      res.send({
        status: response
      })
    })
    .catch(err => {
      console.log(err)
    })
})

app.post('/ticket_phone_credits', (req, res) => {
  ticket_phone_credits(req.body.order)
    .then(response => {
      res.send({
        status: response
      })
    })
    .catch(err => {
      console.log(err)
    })
})

app.post('/emida_error', (req, res) => {
  emida_error(req.body.order)
    .then(response => {
      res.send({
        status: response
      })
    })
    .catch(err => {
      console.log(err)
    })
})

app.post('/open_cd', (req, res) => {
  openCd(req.body.order)
    .then(response => {
      res.send({
        status: response
      })
    })
    .catch(err => {
      console.log(err)
    })
})

app.post('/shutdown', (req, res) => {
  var exec = require('child_process').exec
  exec("shutdown -h now")
})

app.post('/reboot', (req, res) => {
  var exec = require('child_process').exec
  exec("reboot")
})

app.listen(port, () => {
  console.log(`🖨 Printer listening at http://localhost:${port}`)
})

app.post('/update-printer-server', (req, res) => {
  console.log('update requested')
  var exec = require('child_process').exec
  exec("cd ~/walleat-printer-server && git pull origin main")
  res.send({
    status: 200
  })
})
